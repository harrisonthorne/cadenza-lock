use std::time::Duration;

use sctk::reexports::calloop::EventLoop;
use smithay_client_toolkit as sctk;
use state::CadenzaLockState;

mod state;
mod wgpu;

fn main() {
    let mut event_loop: EventLoop<CadenzaLockState> =
        EventLoop::try_new().expect("failed to initialize event loop");

    let mut state = CadenzaLockState::new(&event_loop);

    loop {
        event_loop
            .dispatch(Duration::from_millis(16), &mut state)
            .unwrap();

        if state.exit {
            break;
        }
    }
}
