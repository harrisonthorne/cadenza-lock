use std::{collections::HashMap, time::Duration};

use once_cell::sync::Lazy;
use pollster::FutureExt;
use smithay_client_toolkit::{
    compositor::{CompositorHandler, CompositorState},
    delegate_compositor, delegate_output, delegate_registry, delegate_session_lock, delegate_shm,
    output::{OutputHandler, OutputState},
    reexports::{
        calloop::{
            timer::{TimeoutAction, Timer},
            EventLoop, LoopHandle,
        },
        calloop_wayland_source::WaylandSource,
        client::{
            delegate_noop,
            protocol::{wl_buffer, wl_output, wl_surface},
            Connection, QueueHandle,
        },
    },
    registry::{ProvidesRegistryState, RegistryState},
    registry_handlers,
    session_lock::{
        SessionLock, SessionLockHandler, SessionLockState, SessionLockSurface,
        SessionLockSurfaceConfigure,
    },
    shm::{Shm, ShmHandler},
};
use wayland_client::{
    globals::registry_queue_init,
    protocol::{wl_output::WlOutput, wl_surface::WlSurface},
};

use crate::wgpu::{get_window_handle_pair, WgpuSurfaceBundle};

static WGPU_INSTANCE: Lazy<wgpu::Instance> = Lazy::new(|| {
    wgpu::Instance::new(wgpu::InstanceDescriptor {
        backends: wgpu::Backends::all(),
        ..Default::default()
    })
});

pub(crate) struct CadenzaLockState {
    /// The event loop handle.
    pub loop_handle: LoopHandle<'static, Self>,

    /// The wayland connection.
    pub conn: Connection,

    /// The compositor state.
    pub compositor_state: CompositorState,

    /// The output state.
    pub output_state: OutputState,

    /// The registry state.
    pub registry_state: RegistryState,

    /// The shared memory global.
    pub shm: Shm,

    /// The session lock. This must be an Option so that it can be dropped when
    /// the compostior is unlocked.
    pub session_lock: Option<SessionLock>,

    /// Lock surfaces stored while the lock application is active.
    pub lock_surfaces: HashMap<WlSurface, (SessionLockSurface, WgpuSurfaceBundle<'static>)>,

    /// Whether the lock application should exit.
    pub exit: bool,
}

impl CadenzaLockState {
    pub fn new(event_loop: &EventLoop<'static, Self>) -> Self {
        let conn = Connection::connect_to_env().unwrap();

        let (globals, event_queue) = registry_queue_init(&conn).unwrap();
        let qh: QueueHandle<CadenzaLockState> = event_queue.handle();

        // lock now
        let session_lock = SessionLockState::new(&globals, &qh)
            .lock(&qh)
            .expect("ext-session-lock is not supported with this compositor");

        WaylandSource::new(conn.clone(), event_queue)
            .insert(event_loop.handle())
            .unwrap();

        CadenzaLockState {
            loop_handle: event_loop.handle(),
            conn: conn.clone(),
            compositor_state: CompositorState::bind(&globals, &qh).unwrap(),
            output_state: OutputState::new(&globals, &qh),
            registry_state: RegistryState::new(&globals),
            shm: Shm::bind(&globals, &qh).unwrap(),
            session_lock: Some(session_lock),
            lock_surfaces: HashMap::new(),
            exit: false,
        }
    }
}

impl SessionLockHandler for CadenzaLockState {
    fn locked(&mut self, conn: &Connection, qh: &QueueHandle<Self>, session_lock: SessionLock) {
        println!("locked!");

        for output in self.output_state.outputs() {
            let surface = self.compositor_state.create_surface(qh);
            let lock_surface = session_lock.create_lock_surface(surface, &output, qh);
            let window_handle_pair = get_window_handle_pair(conn, lock_surface.wl_surface());
            let wgpu_surface_bundle =
                WgpuSurfaceBundle::new(&WGPU_INSTANCE, window_handle_pair).block_on();

            self.lock_surfaces.insert(
                lock_surface.wl_surface().clone(),
                (lock_surface, wgpu_surface_bundle),
            );
        }

        // After 1 second, destroy lock
        self.loop_handle
            .insert_source(
                Timer::from_duration(Duration::from_secs(1)),
                |_, _, state| {
                    // Destroy the session lock
                    state.session_lock.take();
                    // Sync connection to make sure compostor receives destroy
                    state.conn.roundtrip().unwrap();
                    // Then we can exit
                    state.exit = true;
                    TimeoutAction::Drop
                },
            )
            .unwrap();
    }

    fn finished(
        &mut self,
        _conn: &Connection,
        _qh: &QueueHandle<Self>,
        _session_lock: SessionLock,
    ) {
        println!("Finished");
        self.exit = true;
    }

    fn configure(
        &mut self,
        _conn: &Connection,
        _qh: &QueueHandle<Self>,
        session_lock_surface: SessionLockSurface,
        configure: SessionLockSurfaceConfigure,
        _serial: u32,
    ) {
        let (width, height) = configure.new_size;
        self.lock_surfaces
            .get_mut(session_lock_surface.wl_surface())
            .unwrap()
            .1
            .configure(width, height);
    }
}

impl CompositorHandler for CadenzaLockState {
    fn scale_factor_changed(
        &mut self,
        _conn: &Connection,
        _qh: &QueueHandle<Self>,
        _surface: &wl_surface::WlSurface,
        _new_factor: i32,
    ) {
    }

    fn transform_changed(
        &mut self,
        _conn: &Connection,
        _qh: &QueueHandle<Self>,
        _surface: &wl_surface::WlSurface,
        _new_transform: wl_output::Transform,
    ) {
    }

    fn frame(
        &mut self,
        _conn: &Connection,
        _qh: &QueueHandle<Self>,
        _surface: &wl_surface::WlSurface,
        _time: u32,
    ) {
    }
}

impl OutputHandler for CadenzaLockState {
    fn output_state(&mut self) -> &mut OutputState {
        &mut self.output_state
    }

    fn new_output(&mut self, _conn: &Connection, _qh: &QueueHandle<Self>, _output: WlOutput) {}

    fn update_output(&mut self, _conn: &Connection, _qh: &QueueHandle<Self>, _output: WlOutput) {}

    fn output_destroyed(&mut self, _conn: &Connection, _qh: &QueueHandle<Self>, _output: WlOutput) {
    }
}

impl ProvidesRegistryState for CadenzaLockState {
    registry_handlers![OutputState,];

    fn registry(&mut self) -> &mut RegistryState {
        &mut self.registry_state
    }
}

impl ShmHandler for CadenzaLockState {
    fn shm_state(&mut self) -> &mut Shm {
        &mut self.shm
    }
}

delegate_compositor!(CadenzaLockState);
delegate_output!(CadenzaLockState);
delegate_session_lock!(CadenzaLockState);
delegate_shm!(CadenzaLockState);
delegate_registry!(CadenzaLockState);
delegate_noop!(CadenzaLockState: ignore wl_buffer::WlBuffer);
