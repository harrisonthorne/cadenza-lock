use std::ptr::NonNull;

use wayland_client::{protocol::wl_surface::WlSurface, Connection, Proxy};
use wgpu::rwh::{
    DisplayHandle, HandleError, HasDisplayHandle, HasWindowHandle, RawDisplayHandle,
    RawWindowHandle, WaylandDisplayHandle, WaylandWindowHandle, WindowHandle,
};

#[derive(Copy, Clone)]
pub struct WindowHandlePair<'d, 'w>(DisplayHandle<'d>, WindowHandle<'w>);

impl<'d> HasDisplayHandle for WindowHandlePair<'d, '_> {
    fn display_handle(&self) -> Result<DisplayHandle<'d>, HandleError> {
        Ok(self.0)
    }
}

impl<'w> HasWindowHandle for WindowHandlePair<'_, 'w> {
    fn window_handle(&self) -> Result<WindowHandle<'w>, HandleError> {
        Ok(self.1)
    }
}

impl<'a> WindowHandlePair<'a, 'a> {
    pub fn create_surface(self, instance: &'a wgpu::Instance) -> wgpu::Surface {
        instance.create_surface(self).unwrap()
    }
}

pub struct WgpuSurfaceBundle<'a> {
    surface: wgpu::Surface<'a>,
    device: wgpu::Device,
    queue: wgpu::Queue,
    adapter: wgpu::Adapter,
}

impl<'a> WgpuSurfaceBundle<'a> {
    pub async fn new(
        instance: &'a wgpu::Instance,
        window_handle_pair: WindowHandlePair<'a, 'a>,
    ) -> Self {
        let surface = window_handle_pair.create_surface(instance);
        let adapter = get_adapter(&surface, instance).await;
        let (device, queue) = adapter
            .request_device(&Default::default(), None)
            .await
            .expect("failed to request wgpu device");

        Self {
            surface,
            device,
            queue,
            adapter,
        }
    }

    pub fn configure(&mut self, width: u32, height: u32) {
        let capabilities = self.surface.get_capabilities(&self.adapter);
        let surface_config = wgpu::SurfaceConfiguration {
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
            format: capabilities.formats[0],
            view_formats: vec![capabilities.formats[0]],
            alpha_mode: wgpu::CompositeAlphaMode::Auto,
            width,
            height,
            desired_maximum_frame_latency: 2,

            // Wayland is inherently a mailbox system.
            present_mode: wgpu::PresentMode::Mailbox,
        };

        self.surface.configure(&self.device, &surface_config);

        // We don't plan to render much in this example, just clear the surface.
        let surface_texture = self
            .surface
            .get_current_texture()
            .expect("failed to acquire next swapchain texture");
        let texture_view = surface_texture
            .texture
            .create_view(&wgpu::TextureViewDescriptor::default());

        let mut encoder = self.device.create_command_encoder(&Default::default());
        {
            let _renderpass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                label: None,
                color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                    view: &texture_view,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Clear(wgpu::Color::BLUE),
                        store: wgpu::StoreOp::Store,
                    },
                })],
                depth_stencil_attachment: None,
                timestamp_writes: None,
                occlusion_query_set: None,
            });
        }

        // Submit the command in the queue to execute
        self.queue.submit(Some(encoder.finish()));
        surface_texture.present();
    }
}

pub fn get_window_handle_pair<'d, 'w>(
    conn: &Connection,
    surface: &WlSurface,
) -> WindowHandlePair<'d, 'w> {
    let raw_display_handle = RawDisplayHandle::Wayland(WaylandDisplayHandle::new(
        NonNull::new(conn.backend().display_ptr() as *mut _).unwrap(),
    ));
    let raw_window_handle = RawWindowHandle::Wayland(WaylandWindowHandle::new(
        NonNull::new(surface.id().as_ptr() as *mut _).unwrap(),
    ));

    let display_handle = unsafe { DisplayHandle::borrow_raw(raw_display_handle) };
    let window_handle = unsafe { WindowHandle::borrow_raw(raw_window_handle) };

    WindowHandlePair(display_handle, window_handle)
}

pub async fn get_adapter<'a>(
    surface: &'a wgpu::Surface<'a>,
    instance: &'a wgpu::Instance,
) -> wgpu::Adapter {
    instance
        .request_adapter(&wgpu::RequestAdapterOptions {
            power_preference: wgpu::PowerPreference::None,
            compatible_surface: Some(surface),
            ..Default::default()
        })
        .await
        .unwrap()
}

// pain
unsafe impl Send for WindowHandlePair<'_, '_> {}
unsafe impl Sync for WindowHandlePair<'_, '_> {}
